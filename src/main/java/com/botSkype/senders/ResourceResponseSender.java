package com.botSkype.senders;

import com.microsoft.bot.connector.Conversations;
import com.microsoft.bot.schema.models.Activity;
import com.microsoft.bot.schema.models.ResourceResponse;
import org.springframework.stereotype.Component;

@Component
public class ResourceResponseSender {

  private ResourceResponseSender(){

  }

	public static ResourceResponse send(Conversations conversations, String conversationId, Activity responseActivity) {
    return conversations.sendToConversation(conversationId, responseActivity);
  }
}
