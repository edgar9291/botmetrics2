package com.botSkype.config;

import com.microsoft.bot.connector.customizations.CredentialProvider;
import com.microsoft.bot.connector.customizations.CredentialProviderImpl;
import com.microsoft.bot.connector.customizations.MicrosoftAppCredentials;
import com.microsoft.bot.schema.models.ResourceResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class BotConfig {
	@Value("${microsft.appId}")
	private String appId;
		
	@Value("${microsft.appPassword}")
	private String appPassword;

	
	public static MicrosoftAppCredentials credentials;
	public static CredentialProvider credentialProvider;
	
	@Bean(name = "credentials")
	public String getCredentials() {
		credentials = new MicrosoftAppCredentials(appId, appPassword);
		credentialProvider = new CredentialProviderImpl(appId, appPassword);
		return "";
	}

	
}
