package com.botSkype.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.botSkype.config.BotConfig;
import com.botSkype.creators.ConversationCreator;
import com.botSkype.models.Conversation;
import com.botSkype.senders.ResourceResponseSender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.joda.deser.DateTimeDeserializer;
import com.microsoft.aad.adal4j.AuthenticationException;
import com.microsoft.bot.connector.ConnectorClient;
import com.microsoft.bot.connector.Conversations;
import com.microsoft.bot.connector.customizations.JwtTokenValidation;
import com.microsoft.bot.connector.implementation.ConnectorClientImpl;
import com.microsoft.bot.schema.models.Activity;
import com.microsoft.bot.schema.models.ActivityTypes;
import com.microsoft.bot.schema.models.ChannelAccount;
import com.microsoft.bot.schema.models.ResourceResponse;

@RestController
@RequestMapping(path = "/api")
public class BotMessagesHandler {
	
	@Value("${microsft.activity.ServiceUrl}")
	private String activityServiceUrl;
	
	@Value("${microsft.botId}")
	private String botId;
	
	@Value("${microsft.botName}")
	private String botName;
		
	private static List<ResourceResponse> responses=new ArrayList<ResourceResponse>();
	private static Map<String,Conversation> accounts = new HashMap<String,Conversation>();

	private Conversations conversation;
	
	private final String messageWellcome = "Que pedo NAME !!!!!";
	private final String messageConversation = "Dijiste: ";
	private final String proactiveMessages = "Huuuuevosss";

		
	
	 	
	@InitBinder
	void createConnector() {
		ConnectorClient connector = new ConnectorClientImpl(activityServiceUrl, BotConfig.credentials);		
		this.conversation = ConversationCreator.createResponseConversation(connector);
	}
	
	@PostMapping(path = "/messages")
	public List<ResourceResponse> create(@RequestHeader("Authorization") String authorization,
			@RequestBody @Valid @JsonDeserialize(using = DateTimeDeserializer.class) Activity activity) throws InterruptedException, ExecutionException, JsonProcessingException {
		
		System.out.println("\n ---------------- URL /api/messages\n" + new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(activity));
		
		JwtTokenValidation.assertValidActivity(activity, authorization, BotConfig.credentialProvider);
		
		ChannelAccount channelAccount = new ChannelAccount();
		channelAccount.withId(activity.from().id());
		channelAccount.withName(activity.from().name());
		accounts.put(activity.from().id(), new Conversation(activity.conversation().id(), channelAccount));

		String sendMessage=messageWellcome.replaceFirst("NAME", activity.from().name());		
		if(activity!=null && activity.text()!=null && !activity.text().isEmpty())
			sendMessage=messageConversation + activity.text();
		
		Activity responseActivity = new Activity().withType(ActivityTypes.MESSAGE).withRecipient(activity.from()).withFrom(activity.recipient()).withText(sendMessage);
		
		ResourceResponse echoResponse = ResourceResponseSender.send(conversation, activity.from().id(), responseActivity);
		responses.add(echoResponse);
		
		return responses;
	}
	
	
	@GetMapping(path = "/proactiveMessages/{userId}")
	public List<ResourceResponse> create(@PathVariable String userId) throws AuthenticationException, InterruptedException, ExecutionException, JsonProcessingException {
		
		System.out.println("\n ---------------- URL /api/proactiveMessages?userId="+userId);
		
		Conversation account = accounts.get(userId);
				
		ChannelAccount channelAccount1 = new ChannelAccount();
		channelAccount1.withId(botId);
		channelAccount1.withName(botName);		
		
		Activity responseActivity = new Activity().withType(ActivityTypes.MESSAGE).withRecipient(account.getChannelAccount()).withFrom(channelAccount1).withText(proactiveMessages);
		
		ResourceResponse echoResponse = ResourceResponseSender.send(conversation, account.getConversationId(), responseActivity);
		responses.add(echoResponse);

		return responses;
	}
	
	
}
