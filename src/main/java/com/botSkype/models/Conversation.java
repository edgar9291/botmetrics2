package com.botSkype.models;

import com.microsoft.bot.schema.models.ChannelAccount;

public class Conversation {
	
	private String conversationId;
	private ChannelAccount channelAccount;
	
	public Conversation(String conversationId, ChannelAccount channelAccount) {
		super();
		this.conversationId = conversationId;
		this.channelAccount = channelAccount;
	}
	public String getConversationId() {
		return conversationId;
	}
	public void setConversationId(String conversationId) {
		this.conversationId = conversationId;
	}
	public ChannelAccount getChannelAccount() {
		return channelAccount;
	}
	public void setChannelAccount(ChannelAccount channelAccount) {
		this.channelAccount = channelAccount;
	}

}
